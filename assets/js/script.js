var sortUrl = '/routes/sort.php';

var rewriteTableWithData = function (tickerData) {
	var html = '';
	html += '<tbody>';
	$.each(tickerData, function( key, value ) {
		html += '<tr>';
		html += '<td class="currency-column">' + key + '</td>';
		html += '<td class="symbol-column">' + value.symbol + '</td>';
		html += '<td class="fifteen-column">' + value['15m'] + '</td>';
		html += '<td class="last-column">' + value.last + '</td>';
		html += '<td class="buy-column">' + value.buy + '</td>';
		html += '<td class="sell-column">' + value.sell + '</td>';
		html += '</tr>';
	});

	html += '</tbody>';
	$("#ticker-table").append(html);
};

var clearTableData = function () {
	$("#ticker-table").find("tbody").remove();
};

var getTickerData = function () {
	return $.post(sortUrl, {sort: getSortValue(), limitValue: getLimitValue()}, function (data) {
			return data;
		})
		.fail(function() {
			alert('An error has occurred while retrieving data.')
		})
};

var clearAndRewriteTable = function (tickerData) {
	clearTableData();
	rewriteTableWithData(JSON.parse(tickerData));
};

var getLimitValue = function() {
	return $("#limit-results").val();
};

var getSortValue = function () {
	return $("#sort-selection").val();
};

$("#limit-results, #sort-selection").change(function () {
	getTickerData().then(function (tickerData) {
		clearAndRewriteTable(tickerData);
	});
});

$("#refresh-button").click(function () {
	getTickerData().then(function (tickerData) {
		clearAndRewriteTable(tickerData);
	});
});