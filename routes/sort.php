<?php

require_once '../vendor/autoload.php';

use Topview\TickerFetcher;
use Topview\TickerSorter;

$fetcher = new TickerFetcher();
$sorter = new TickerSorter();
$sortBy = '';

$tickerData = $fetcher->fetchTickerData();
$tickerDataArray = json_decode($tickerData, true);

if(isset($_POST['sort'])) {
	$sortBy = $_POST['sort'];
}

if(isset($_POST['limitValue'])&&(is_numeric($_POST['limitValue']))) {
	$limit = $_POST['limitValue'];
} else {
	$limit = count($tickerDataArray);
}

switch ($sortBy) {
	case 'bySellPrice':
		$sortedTickerData = $sorter->sortBySellPrice($tickerDataArray);
		break;
	case 'byName':
		$sortedTickerData = $sorter->sortAlphabetically($tickerDataArray);
		break;
	default:
		$sortedTickerData = $tickerDataArray;
}

$sortedAndLimitedData = $sorter->limitResults($sortedTickerData, $limit);

print(json_encode($sortedAndLimitedData));

