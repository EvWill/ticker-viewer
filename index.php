<?php

require_once 'vendor/autoload.php';
use Topview\TickerFetcher;

// Setup Twig
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array('debug' => true));
$twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) {
	return sprintf('/%s', ltrim($asset, '/'));
}));

//Load and render template with data
$template = $twig->load('index.html');
$fetcher = new TickerFetcher();
$tickerData = $fetcher->fetchTickerData();
echo $template->render(array('tickerData' => json_decode($tickerData, true)));


