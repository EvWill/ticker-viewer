<?php namespace Topview;

class TickerSorter {

	public function sortBySellPrice($tickerData) {
		uasort($tickerData, array($this, 'sortTickerDataBySellPrice'));
		return $tickerData;
	}

	public function sortAlphabetically($tickerData) {
		ksort($tickerData);
		return $tickerData;
	}

	public function limitResults($tickerData, $limit) {
		return array_slice($tickerData, 0, $limit);
	}

	private function sortTickerDataBySellPrice($a, $b) {
		return $b['sell'] - $a['sell'];
	}

}