<?php namespace Topview;

class TickerFetcher {

	const TICKER_ENDPOINT = 'https://blockchain.info/ticker';

	public function fetchTickerData() {
		return file_get_contents(self::TICKER_ENDPOINT);
	}

}